#!/usr/bin/python3
import pandas as pd
import matplotlib.pyplot as plt

#https://debianforum.de/forum/viewtopic.php?f=34&t=171699&sid=2fb3765a5652b59de49deb22102672f6

def plotVisitorsperDay(df,name):
    plt.figure()
    df.plot(kind='line', title=name, x='date', figsize=(10,5)).get_figure().savefig(name+'.png')
    
def plotAverageVisitorsperHour(df, name):
    plt.figure()
    df.groupby('time')['visitors'].agg('mean').plot(kind='bar', title=name, figsize=(10,5)).get_figure().savefig(name+'.png')

def plotAverageVisitorsperWeekday(df,name):
    plt.figure()
    catego = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday', 'Sunday']
    df =  df.groupby(df.date.dt.weekday_name)['visitors'].agg('mean')
    df.index = pd.Categorical(df.index, categories=catego, ordered=True)
    df = df.sort_index()
    ax = df.plot(kind='bar', title=name, figsize=(10,8))
    ax.set_xlabel("Weekday")
    ax.get_figure().savefig(name+'.png')

def plotVisitorsperMonth(df,name):
    plt.figure()
    df.groupby(df.date.dt.month_name())['visitors'].agg('sum').sort_index(ascending=False).plot(kind='bar', title=name, figsize=(10,8)).get_figure().savefig(name+'.png')

def filterOutliers(log):
    #log.visitors.isna().sum() --> returns 2
    log = log[log['visitors'].notnull()]

    log['visitors'] = log.visitors.astype('int')
    
    #removing outliers based on normal distribution and standard deviation
    #log.sort_values('visitors', ascending=False).head(20 )
    mean  = log['visitors'].mean()
    sd = log['visitors'].std()
    log =  log[(log.visitors < mean + 2 *sd) & (log.visitors > mean - 2 *sd)]
    
    return log

def main():  
    labels = ['ts', 'fetchtime', 'visitors']
    log = pd.read_csv('dfde-besucher.log', delimiter='\t', header=None, names=labels)

    #cleaning up the data
    log = log.drop('fetchtime',1)
    log['visitors'] = log.visitors.str.replace(' Besucher online','')

    #splitting up the timestamp
    log[['date','time']] = log['ts'].str.split(' ', 1, expand=True)
    log = log.drop('ts',1)

    #removing outliers as they make most diagrams useless
    log = filterOutliers(log)

    #Plotting
    plotVisitorsperDay(log, 'Besucherlog')
    plotAverageVisitorsperHour(log, 'DurchschnittBesucherStunde')
    
    #I'm not sure why I can't do this before
    log['date'] = pd.DatetimeIndex(data=log.date)
    plotAverageVisitorsperWeekday(log,'DurchschnittBesucherWochentag')
    plotVisitorsperMonth(log,'BesucherMonat')

if __name__ == "__main__":
    main()

